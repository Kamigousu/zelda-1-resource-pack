               ~SOLARUS - THE NES PACK~
                         /\
                        /  \
                       /    \
                      /      \
                     /        \
                    /__________\
                   /\__________/\
                  /  \        /  \
                 /    \      /    \
                /      \    /      \
               /        \  /        \
              /__________\/__________\
              \__________/\__________/
      

Hey! Thanks for downloading this pack! We'd like to give
some special thanks to the people who made this pack possible!

		         The Zelda 1 Team:
	        H. Miyauchi (Hiroshi Yamauchi)
	        S. Miyahon (Shigeru Miyamoto)
	          Ten Ten (Takashi Tezuka)
	         T. Nakazoo (Toshihiko Nakago)
	         Yachan (Yasunari Soejima)
		         Marumaru (I. Marui)
	    	    Konchan (Koji Kondo)

			            &
 
                     Kamigouso
                   CopperKatana
                  Breandan Maloney
		          IsabelleChiming
                  The Solarus Team

			            &
			
	        Solarus Developers like you.

Music Provided by IsabelleChiming

You can find the Zelda Remade Album and much of her other work here:
https://isabellechiming.bandcamp.com/music
https://www.youtube.com/channel/UCEUvrYSL7vyFjjWYLyqnlhA

Additional Music and SFX provided by Breandan Maloney

You can find him, as well as the rest of the Solarus Team, on the 
Solarus Discord:
https://discord.gg/yYHjJHt

			          ~~~
		          Legal Stuff

Most of the assets in this pack are derivatives of assets by Nintendo,
made without the permission of Nintendo or the developers who made them.
As such, the Solarus Team does not own the rights to their use or distribution.
It is recommended that any use of this pack or its assets are
restricted to non-commercial use, as to not draw the ire of several very
rich Japanese people and their lawyers. 

			         ~~~
		          How to Use

This pack is for use with the Solarus Game Editor. Simply load it as a
project to use, and use it like any other resource pack.
