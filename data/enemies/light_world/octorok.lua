-- Octorok: shoots stones.

local enemy = ...

local children = {}

local can_shoot = true

function enemy:on_created()

  enemy:set_life(1)
  enemy:set_damage(2)
  enemy:create_sprite("enemies/" .. enemy:get_breed())
end

local function go_hero()

  local sprite = enemy:get_sprite()
  sprite:set_animation("walking")
  local movement = sol.movement.create("random_path")
  movement:set_speed(48)
  movement:start(enemy)
end

local function shoot()

  local map = enemy:get_map()
  local hero = map:get_hero()
  if not enemy:is_in_same_region(hero) then
    return true  -- Repeat the timer.
  end

  local sprite = enemy:get_sprite()
  sprite:set_direction(enemy:get_direction4_to(hero))  
  local x, y, layer = enemy:get_position()
  local direction = sprite:get_direction()

  -- Where to create the projectile.
  local dxy = {
    {  8,  -6 },
    {  0, -13 },
    { -8,  -6 },
    {  0,   0 },
  }

  
    
  sprite:set_animation("shooting")
  enemy:stop_movement()
  sol.timer.start(enemy, 300, function()
    sol.audio.play_sound("stone")
--[[
    local stone = enemy:create_enemy({  --TODO: change the stone from an enemy to a custom entity to avoid the sword beam.
      breed = "others/octorok_stone",
      x = dxy[direction + 1][1],
      y = dxy[direction + 1][2],
    })
  --]]
    local map = enemy:get_map()

    local source_x, source_y, source_layer = enemy:get_position()    
    
    local stone = map:create_custom_entity({
      
      x = source_x + dxy[direction+1][1],
      y = source_y + dxy[direction+1][2],
      model = "octorok_stone",
      layer = enemy:get_layer(),
      direction = 0,
      height = 16,
      width = 16,
      sprite = "enemies/others/octorok_stone",
    })  


    children[#children + 1] = stone
    stone:go(direction)

    sol.timer.start(enemy, 500, go_hero)
  end)
  
end

function enemy:on_restarted()

  local map = enemy:get_map()
  local hero = map:get_hero()

  go_hero()

  can_shoot = true

  sol.timer.start(enemy, 100, function()

    local hero_x, hero_y = hero:get_position()
    local x, y = enemy:get_center_position()

    if can_shoot then
      local aligned = (math.abs(hero_x - x) < 16 or math.abs(hero_y - y) < 16)
      if aligned and enemy:get_distance(hero) < 200 then
        shoot()
        can_shoot = false
        sol.timer.start(enemy, 1500, function()
          can_shoot = true
        end)
      end
    end
    return true  -- Repeat the timer.
  end)
end

function enemy:on_movement_changed(movement)

  local direction4 = movement:get_direction4()
  local sprite = self:get_sprite()
  sprite:set_direction(direction4)
end

local previous_on_removed = enemy.on_removed
function enemy:on_removed()

  if previous_on_removed then
    previous_on_removed(enemy)
  end

  for _, child in ipairs(children) do
    child:remove()
  end
end
