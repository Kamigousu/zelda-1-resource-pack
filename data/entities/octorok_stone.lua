-- Lua script of custom entity octorok_stone.
-- This script is executed every time a custom entity with this model is created.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation for the full specification
-- of types, events and methods:
-- http://www.solarus-games.org/doc/latest

local entity = ...
local game = entity:get_game()
local map = entity:get_map()

-- Event called when the custom entity is initialized.
function entity:on_created()

  --entity.sprite = sol.sprite.create("enemies/others/octorok_stone")
  entity:set_size(8, 8)
  entity:set_origin(4, 4)
  entity:set_can_traverse_ground("shallow_water",true)
  entity:set_can_traverse_ground("deep_water",true)
  entity:set_can_traverse_ground("lava",true)
  entity:set_can_traverse_ground("hole",true)
  entity:set_can_traverse_ground("wall",true)
  entity:set_can_traverse("hero",true)
  -- Initialize the properties of your custom entity here,
  -- like the sprite, the size, and whether it can traverse other
  -- entities and be traversed by them.
  
  entity:add_collision_test("overlapping", function(entity, other_entity)
    if (other_entity:get_type() == "hero") then
      if (other_entity:is_blinking() == false) then
        local entity_direction = entity:get_sprite():get_direction()
        local o_entity_direction = other_entity:get_sprite():get_direction()
        if (entity:get_game():get_ability("shield") > 0) and (opposing(entity_direction, o_entity_direction)) then
          --When the hero has a shield and the directions of the shield and projectile are opposing          
          sol.audio.play_sound("shield")          
          entity:remove()
        else
          other_entity:start_hurt(entity, entity:get_sprite(), 2)
        end
        
      end
    end
  end)  
end

--[[
function entity:destroy()
  entity:get_movement():stop()
  entity:clear_collision_tests()
  entity:remove_sprite()
  entity:create_sprite("enemies/enemy_killed")
  sol.timer.start(600, function()
    entity:remove()
  end)
end
--]]

function opposing(num1, num2)
  if num1 == 0 then
    if num2 == 2 then
      return true
    else
      return false
    end
  elseif num1 == 2 then
    if num2 == 0 then
      return true
    else
      return false
    end
  elseif num1 == 1 then
    if num2 == 3 then
      return true
    else
      return false
    end
  elseif num1 == 3 then
    if num2 == 1 then
      return true
    else
      return false
    end
  end
end

function entity:go(direction4)

  local angle = direction4 * math.pi / 2
  local movement = sol.movement.create("straight")
  movement:set_speed(192)
  movement:set_angle(angle)
  movement:set_smooth(false)
  movement:start(entity)

  entity:get_sprite():set_direction(direction4)
end

entity:register_event("on_obstacle_reached", function()
  entity:remove()
end)