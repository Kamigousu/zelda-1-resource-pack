local item = ...

function item:on_created()

  self:set_sound_when_picked(nil)
  self:set_sound_when_brandished("heart_container")
end

function item:on_obtained(variant, savegame_variable)

  local game = self:get_game()

  local old_volume = sol.audio.get_music_volume()
  sol.audio.set_music_volume(50)     --Quiets the background music when the dialog box appears.
  
  game:start_dialog("found_heart_container", function()
    game:add_max_life(4)
    game:set_life(game:get_max_life())
    sol.audio.set_music_volume(old_volume)
  end)
end

