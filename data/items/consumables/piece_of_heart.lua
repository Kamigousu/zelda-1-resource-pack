local item = ...

local message_id = {
  "found_piece_of_heart.first",
  "found_piece_of_heart.second",
  "found_piece_of_heart.third",
  "found_piece_of_heart.fourth"
}

function item:on_created()

  self:set_sound_when_picked(nil)
  self:set_sound_when_brandished("piece_of_heart")
  self:set_savegame_variable("pieces_of_heart")
 
end


function item:on_obtained(variant, savegame_variable)

  local game = self:get_game()
  local nb_pieces_of_heart = game:get_value("i1030") or 0
  
  local old_volume = sol.audio.get_music_volume()
  sol.audio.set_music_volume(50)     --Quiets the background music when the dialog box appears.
  
  
  game:start_dialog(message_id[nb_pieces_of_heart + 1], function()
    game:set_value("i1030", (nb_pieces_of_heart + 1) % 4)
    if nb_pieces_of_heart == 3 then
      game:add_max_life(4)
    end
    game:add_life(game:get_max_life())
    sol.audio.set_music_volume(old_volume)
  end)
  
end

function item:on_pickable_created(pickable)
  local game = self:get_game()
  local nb_pieces_of_heart = game:get_value("i1030") or 0
  local sprite = pickable:get_sprite()
  sprite:set_direction(nb_pieces_of_heart)
end