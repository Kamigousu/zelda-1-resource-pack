--[[08/22/19 This script was merged with the game meta script. Kamigousu



require("scripts/multi_events")  --This is required for this script to work. It allows the script to be called on its own.

local game_meta = sol.main.get_metatable("game")

local function set_camera(game)
  if game ~= nil then
    if game:get_map() ~= nil then
      local map = game:get_map()  
      local camera = map:get_camera()
      camera:set_size(256, 176)
      --camera:set_position_on_screen(0, 0)
    end
  end
end

game_meta:register_event("on_map_changed", set_camera)
--]]