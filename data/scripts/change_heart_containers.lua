--[[08/22/19 This script was merged with the game meta script. Kamigousu


require("scripts/multi_events")
 
local game_meta = sol.main.get_metatable("game")
 
local function update_entities(game)
  if game ~= nil then
    if game:get_map() ~= nil then
      local map = game:get_map()  
      for entity in map:get_entities_by_type("pickable") do
        local sprite = entity:get_sprite()        
        if sprite:get_animation() == "consumables/piece_of_heart" then        
          local nb_pieces_of_heart = game:get_value("i1030") or 0
          sprite:set_direction(nb_pieces_of_heart)
        end
      end
    end
  end
end
 
 
game_meta:register_event("on_map_changed", update_entities)
--]]