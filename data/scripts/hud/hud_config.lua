-- Defines the elements to put in the HUD
-- and their position on the game screen.

-- You can edit this file to add, remove or move some elements of the HUD.

-- Each HUD element script must provide a method new()
-- that creates the element as a menu.
-- See for example scripts/hud/hearts.lua.

-- Negative x or y coordinates mean to measure from the right or bottom
-- of the screen, respectively.

local hud_y_offset = 160

local hud_config = {

  -- Overlay
  {
    menu_script = "scripts/hud/overlay",
    x = 0,
    y = 176,
  },
  
  -- Item assigned to slot 1.
  {
    menu_script = "scripts/hud/magic_meter",
    x = 10,
    y = 24 + hud_y_offset,
    slot = 1,  -- Item slot (1 or 2).
  },

  -- Item assigned to slot 1.
  {
    menu_script = "scripts/hud/item",
    x = 127,
    y = 30 + hud_y_offset,
    slot = 1,  -- Item slot (1 or 2).
  },
  
  --Sword icon
  {
    menu_script = "scripts/hud/sword_icon",
    x = 151,
    y = 30 + hud_y_offset,

  },

  -- Rupee counter.
  {
    menu_script = "scripts/hud/rupees",
    x = 80,
    y = 28 + hud_y_offset,
  },

 -- Bombs counter
  {
    menu_script = "scripts/hud/bombs",
    x = 80,
    y = 52 + hud_y_offset,
  },

 -- Arrow counter
  {
    menu_script = "scripts/hud/arrows",
    x = 80,
    y = 36 + hud_y_offset,
  },

  -- Small key counter.
  {
    menu_script = "scripts/hud/small_keys",
    x = 80,
    y = 44 + hud_y_offset,
  },

  -- Hearts meter.
  {
    menu_script = "scripts/hud/hearts",
    x = -82,
    y = 54 + hud_y_offset,
  },
  
  {
    menu_script = "scripts/hud/minimap",
    x = 10,
    y = hud_y_offset + 28,
  },

  -- You can add more HUD elements here.
}

return hud_config
