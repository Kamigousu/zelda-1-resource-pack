local builder = {}

local map_img = sol.surface.create("hud/map_background.png")
local player_img = sol.surface.create("hud/map_player_icon.png")

function builder:new(game, config)
  
  local minimap = {}
  
  function minimap:on_draw(dst_surface)
    local x, y = config.x, config.y
    map_img:draw(dst_surface, x, y)
    
    local map = game:get_map()
    if map.player_icon_x ~= nil then
      local extra_x, extra_y = map.player_icon_x, map.player_icon_y
      player_img:draw(dst_surface, x + extra_x, y + extra_y)
    end
    
  end
  return minimap
end

return builder