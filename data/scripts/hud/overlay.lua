local overlay_builder = {}

function overlay_builder:new(game, config)

  local overlay = {}
  
  if config ~= nil then
    overlay.dst_x, overlay.dst_y = config.x, config.y
  end

  overlay.surface = sol.surface.create("hud/overlay.png")
  overlay.transparent = false

  function overlay:on_started()
  
  end
  
  function overlay:on_draw(dst_surface)
    local x, y = overlay.dst_x, overlay.dst_y    
    if not overlay.transparent then     
      overlay.surface:draw(dst_surface, x, y)
    end     
  end

  return overlay
end

return overlay_builder