-- An icon that shows the inventory item assigned to a slot.

local sword_icon_builder = {}

local background_img = sol.surface.create("hud/item_icon.png")

function sword_icon_builder:new(game, config)

  local sword_icon = {}

  --sword_icon.slot = config.slot or 1
  sword_icon.surface = sol.surface.create(18, 30)
  sword_icon.sword_sprite = sol.sprite.create("menus/items/equipment/sword")
  sword_icon.sword_displayed = game:get_value("possession_sword")
  sword_icon.sword_variant_displayed = game:get_ability("sword")

  sword_icon.text = sol.text_surface.create{
    horizontal_alignment = "center",
    vertical_alignment = "top",
    font = "PressStart2P",
    font_size = 8,
    color = {255, 255, 255},
    text = "C",
  }

  local dst_x, dst_y = config.x, config.y

  function sword_icon:rebuild_surface()

    sword_icon.surface:clear()

    -- Background image.
    background_img:draw(sword_icon.surface, 0, 4)
    
    --Text indicating the key to press.
    sword_icon.text:draw(sword_icon.surface, 9, 0)    

    if sword_icon.sword_displayed ~= nil then
      -- Item.
      --sword_icon.surface:fill_color({0, 0, 0}, 3, 3, 16, 16)
      sword_icon.sword_sprite:draw(sword_icon.surface, 9, 21)
    end
  end

  function sword_icon:on_draw(dst_surface)

    local x, y = dst_x, dst_y
    local width, height = dst_surface:get_size()
    if x < 0 then
      x = width + x
    end
    if y < 0 then
      y = height + y
    end

    sword_icon.surface:draw(dst_surface, x, y)
  end

  local function check()

    local need_rebuild = false

    -- Item assigned.
    local sword = game:get_value("possession_sword")
    if sword_icon.sword_displayed ~= sword then
      need_rebuild = true
      sword_icon.sword_displayed = sword
      sword_icon.sword_variant_displayed = nil
      if sword ~= nil then
        sword_icon.sword_sprite = sol.sprite.create("menus/items/equipment/sword")
      end
    end

    if sword ~= nil then
      -- Variant of the sword.
      local sword_variant = game:get_ability("sword")
      if sword_icon.sword_variant_displayed ~= sword_variant then
        need_rebuild = true
        sword_icon.sword_variant_displayed = sword_variant
        sword_icon.sword_sprite:set_direction(sword_variant - 1)
      end
    end

    -- Redraw the surface only if something has changed.
    if need_rebuild then
      sword_icon:rebuild_surface()
    end

    return true  -- Repeat the timer.
  end

  -- Periodically check.
  check()
  sol.timer.start(game, 50, check)
  sword_icon:rebuild_surface()

  return sword_icon
end

return sword_icon_builder


