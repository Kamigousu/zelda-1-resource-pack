local game_over_options_menu = {}

local function create_text(input_text,input_alignment)
  local tsurface = sol.text_surface.create{
    text = input_text,
    alignment = input_alignment,
    font = "PressStart2P",
    font_size = 8, 
  }
  return tsurface
end

game_over_options_menu.surface = sol.surface.create(sol.video.get_quest_size())
local cursor_img = sol.sprite.create("hud/heart")
local phase = "game_over"
local cursor_positions = {30,50,70,90}
local game_over_text = create_text(string.upper(sol.language.get_string("gameover_menu.gameover")), "center") 
local continue_text = create_text(string.upper(sol.language.get_string("gameover_menu.continue")), "center")
local save_text = create_text(string.upper(sol.language.get_string("gameover_menu.save")),"center")
local retry_text = create_text(string.upper(sol.language.get_string("gameover_menu.retry")), "center")
local quit_text = create_text(string.upper(sol.language.get_string("gameover_menu.quit")),"center")
local black_background = sol.surface.create(sol.video.get_quest_size())
black_background:fill_color{0,0,0}
local needs_rebuild = false
local cursor_position = 1
local controls_disabled = false

function game_over_options_menu:on_started()
  controls_disabled = false
  phase = "game_over"
  self:rebuild()
  
  sol.timer.start(2000, function()
    phase = "options"    
    game_over_options_menu:rebuild()
    sol.audio.play_music("IsabelleChiming/IsabelleChiming - The Legend of Zelda Remade - 18 Game Over")
  end)
end

function game_over_options_menu:rebuild()
  self.surface:clear()
  black_background:draw(self.surface)
  local x = 90
  if (phase == "game_over") then
    game_over_text:draw(game_over_options_menu.surface,x,30)
  end
  if (phase == "options") then
    continue_text:draw(game_over_options_menu.surface,x,cursor_positions[1])
    save_text:draw(game_over_options_menu.surface,x,cursor_positions[2])
    retry_text:draw(game_over_options_menu.surface,x,cursor_positions[3])
    quit_text:draw(game_over_options_menu.surface,x,cursor_positions[4])
    cursor_img:draw(game_over_options_menu.surface,x-10,cursor_positions[cursor_position]+3) 
  end
  self.needs_rebuild = false
end

function game_over_options_menu:on_key_pressed(key)
  if (controls_disabled) then
    return
  end  
  
  if (phase == "options") then
    local handled = false
  
    if (key == "down") then
      if (cursor_position < 4) then
        cursor_position = cursor_position + 1
      else
        cursor_position = 1
      end
    sol.audio.play_sound("cursor")
    handled = true
    elseif (key == "up") then
      if (cursor_position > 1) then
        cursor_position = cursor_position - 1
      else
        cursor_position = 4
      end
    sol.audio.play_sound("cursor")
    handled = true
    elseif (key == "space") then
      controls_disabled = true
      sol.audio.stop_music()
      sol.audio.play_sound("ok")
      if (cursor_position == 1) then
        --Continue
        handled = true
        sol.main.get_game():start()
      elseif (cursor_position == 2) then
        --Save
        sol.main.get_game():save()
        sol.main.get_game():start()
      elseif (cursor_position == 3) then  
        --Retru
        handled = true
        sol.main.get_game():set_value("death_count", death_count - 1)
        sol.main.get_game():start()
      elseif  (cursor_position == 4) then 
        --Quit
        handled = true
        sol.main.reset()
      end
    end
  needs_rebuild = true
  return handled
  end  
end

function game_over_options_menu:on_draw(dst_surface)  
  if (needs_rebuild) then
    self:rebuild()
  end
  self.surface:draw(dst_surface)
end

return game_over_options_menu