local savegames_menu = {}

local gui_designer = require("scripts/menus/lib/gui_designer")
local game_manager = require("scripts/game_manager")
local options_menu = require("scripts/menus/options")
--local register_menu = require("scripts/menus/register")
local layout
local savegames_surfaces = {}
local games = {}
local hearts_img = sol.surface.create("hud/hearts.png")
local cursor_img = sol.sprite.create("hud/heart")
local fairy_img = sol.surface.create("menus/fairy_cursor.png")
local cursor_position = 1
local show_savegame_action_box
local show_confirm_delete_box
local link_img = sol.surface.create("menus/link_cursor.png")
local black_surface = sol.surface.create(256,256)
black_surface:fill_color{0,0,0}
local small_black_surface = sol.surface.create(48, 12)
small_black_surface:fill_color{0,0,0}

local black_foreground = sol.surface.create(sol.video.get_quest_size())
black_foreground:fill_color{0,0,0}
black_foreground:set_opacity(100)

local controls_disabled = false

local function get_savegame_file_name(index)

  return "save" .. index .. ".dat"
end

local function get_last_savegame_slot()

  local file = sol.file.open("last_save.dat")
  if file == nil then
    return 1
  end

  local index = file:read("*n")
  file:close()
  if index == nil or index < 1 or index > 3 then
    return 1
  end

  return index
end

local function set_last_savegame_slot(index)

  local file = sol.file.open("last_save.dat", "w")
  file:write(index)
  file:close()
end

local function build_layout()

  layout = gui_designer:create(320, 240)

  layout:make_image(black_surface, 0, 0)
  layout:make_wooden_frame(27, 67, 202, 138)
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.title")), 128, 16, "center")
  --layout:make_wooden_frame(16, 48, 288, 176)
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.options")), 52, 170, "left")
  --layout:make_text("1.", 44, 56)
  --layout:make_text("2.", 44, 104)
  --layout:make_text("3.", 44, 152)
  
  layout:make_image(small_black_surface, 84, 62)
  layout:make_image(small_black_surface, 154, 62)
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.select")), 130, 42, "center")
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.name")), 108, 64, "center")
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.life")), 178, 64, "center")
  layout:make_image(link_img, 52, 90)
  layout:make_image(link_img, 52, 114)
  layout:make_image(link_img, 52, 138)
end

local function draw_hearts(game, game_surface)

  local life = game:get_life()
  local max_life = game:get_max_life()
  
  life = life/2 --Our pack uses hearts with 4 pieces, so adapting this part of the script requires drawing half as many hearts.
  max_life = max_life/2  -- ""
  
  for j = 1, max_life do
    if j % 2 == 0 then
      local x, y
      if j <= 20 then
        x = 40 + 4 * j
        y = 0
      else
        x = 40 + 4 * (j - 20)
        y = 8
      end
      if life >= j then
        hearts_img:draw_region(27, 0, 9, 8, game_surface, x, y)
      else
        hearts_img:draw_region(27, 0, 9, 8, game_surface, x, y)
      end
    end
  end
  if life % 2 == 1 then
    local x, y
    if life <= 20 then
      x = 40 + 4 * (life + 1)
      y = 0
    else
      x = 40 + 4 * (life - 19)
      y = 8
    end
    hearts_img:draw_region(8, 0, 8, 8, game_surface, x, y)
  end
end

local function read_savegames()

  for i = 1, 3 do
    local file_name = get_savegame_file_name(i)
    local surface = sol.surface.create(272, 16)
    surface:set_xy(114, 50 + i * 48)
    savegames_surfaces[i] = surface

    if not sol.game.exists(file_name) then
      games[i] = nil
    else
      -- Existing file.
      local game = game_manager:create(file_name)
      games[i] = game

      -- Hearts.
      draw_hearts(game, surface)
    end
  end
end

-- Places the cursor on the savegame 1, 2 or 3,
-- or on the options (4).
local function set_cursor_position(index)

  cursor_position = index
  cursor_img:set_xy(50, 82 + index * 24)
end

function savegames_menu:on_started()

  sol.audio.play_music("IsabelleChiming/IsabelleChiming - The Legend of Zelda Remade - 02 File Select")
  black_foreground:fade_out(20, nil)
  build_layout()
  read_savegames()
  set_cursor_position(get_last_savegame_slot())
end

function savegames_menu:on_finished()  
  layout = nil
end

function savegames_menu:on_draw(dst_surface)

  layout:draw(dst_surface)
  for i = 1, 3 do
    savegames_surfaces[i]:draw(dst_surface)
  end
  cursor_img:draw(dst_surface)
  black_foreground:draw(dst_surface)
end

function savegames_menu:on_key_pressed(key)
  if (controls_disabled) then
    return
  end  
  
  local handled = false

  if key == "down" then
    if cursor_position < 4 then
      set_cursor_position(cursor_position + 1)
    else
      set_cursor_position(1)
    end
    sol.audio.play_sound("cursor")
    handled = true
  elseif key == "up" then
    if cursor_position > 1 then
      set_cursor_position(cursor_position - 1)
    else
      set_cursor_position(4)
    end
      sol.audio.play_sound("cursor")
      handled = true
    elseif key == "space" then
      if cursor_position <= 3 then
        if games[cursor_position] == nil then
          -- Create a new savegame.
          sol.audio.stop_music()
          sol.audio.play_sound("pause_closed")
          local game = game_manager:create(get_savegame_file_name(cursor_position))
          set_last_savegame_slot(cursor_position)
          controls_disabled = true
          black_foreground:fade_in(20, function()
          sol.main:start_savegame(game)       
          end)
        else
          -- Show actions for an existing savegame.
          show_savegame_action_box(cursor_position)
        end
      elseif cursor_position == 4 then
       -- Options.
        sol.audio.play_sound("pause_closed")
        sol.menu.start(savegames_menu, options_menu)
        function options_menu:on_finished()
          build_layout()  -- Because the language may have changed.
          options_menu.on_finished = nil
        end
      end
      handled = true
    end
    return handled
end

-- Creates a popup that lets the user choose between load, delete or cancel
-- for a savegame.
function show_savegame_action_box(savegame_index)

  local action_box_menu = {}
  local fairy_cursor_position = 1
  local layout = gui_designer:create(112, 72)
  layout:make_wooden_frame()
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.load")), 50, 10, "left")
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.delete")), 50, 30, "left")
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.cancel")), 50, 50, "left")

  function action_box_menu:on_key_pressed(key)
    
  if (controls_disabled) then
    return
  end  
  
    if key == "up" then
      sol.audio.play_sound("cursor")
      if fairy_cursor_position > 1 then
        fairy_cursor_position = fairy_cursor_position - 1
      else
        fairy_cursor_position = 3
      end

    elseif key == "down" then
      sol.audio.play_sound("cursor")
      if fairy_cursor_position < 3 then
        fairy_cursor_position = fairy_cursor_position + 1
      else
        fairy_cursor_position = 1
      end

    elseif key == "space" then

      if fairy_cursor_position == 1 then
        -- Load.
        sol.audio.play_sound("ok")
        set_last_savegame_slot(cursor_position)
        controls_disabled = true
        black_foreground:fade_in(20, function()
          sol.main:start_savegame(games[cursor_position])         
        end)      

      elseif fairy_cursor_position == 2 then
        -- Delete.
        sol.menu.stop(action_box_menu)
        show_confirm_delete_box(function()
          sol.audio.play_sound("pause_open")
          sol.game.delete(get_savegame_file_name(savegame_index))
          read_savegames()
        end)

      else
        -- Cancel.
        sol.audio.play_sound("pause_closed")
        sol.menu.stop(action_box_menu)
      end

    end

    return true
  end

  function action_box_menu:on_draw(dst_surface)

    layout:draw(dst_surface, 104, 84)
    fairy_img:draw(dst_surface, 112, 72 + fairy_cursor_position * 20)
  
    black_foreground:draw(dst_surface)
  end

  gui_designer:map_joypad_to_keyboard(action_box_menu)
  sol.audio.play_sound("pause_closed")
  sol.menu.start(savegames_menu, action_box_menu)
end

-- Creates a popup that ask confirmation to delete something.
function show_confirm_delete_box(action)

  local delete_box_menu = {}
  local fairy_cursor_position = 2
  local layout = gui_designer:create(112, 72)
  layout:make_wooden_frame()
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.delete_question")), 56, 8, "center")
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.yes")), 56, 28, "center")
  layout:make_text(string.upper(sol.language.get_string("savegames_menu.no")), 56, 48, "center")

  function delete_box_menu:on_key_pressed(key)

    if key == "up" or key == "down" then
      sol.audio.play_sound("cursor")
      fairy_cursor_position = 3 - fairy_cursor_position

    elseif key == "space" then

      if fairy_cursor_position == 1 then
        -- Yes: do the action.
        action()
      else
        sol.audio.play_sound("pause_closed")
      end
      sol.menu.stop(delete_box_menu)

    end

    return true
  end

  function delete_box_menu:on_draw(dst_surface)

    layout:draw(dst_surface, 104, 84)
    fairy_img:draw(dst_surface, 112, 92 + fairy_cursor_position * 20)
  end

  gui_designer:map_joypad_to_keyboard(delete_box_menu)

  sol.audio.play_sound("pause_closed")
  sol.menu.start(savegames_menu, delete_box_menu)
end

gui_designer:map_joypad_to_keyboard(savegames_menu)

return savegames_menu
