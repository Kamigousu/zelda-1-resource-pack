--This script builds the title screen.

--The title screen has two "phases" which determine what you're seeing.
--The phases are "main" (the Zelda logo), and "intro" (scrolls the intro text).
--When one phase starts, it activates a series of timers which then activate
--the next phase. The title loops between these two phases until the user
--presses a button.

local title = {}


function title:on_started()
      
  sol.audio.play_music("IsabelleChiming/IsabelleChiming - The Legend of Zelda Remade - 01 Title")
  
  self.surface_w, self.surface_h = sol.video.get_quest_size()
  self.surface = sol.surface.create(self.surface_w, self.surface_h)
  
  self.title_bg = sol.surface.create("menus/title_zelda1/zelda_title_bg.png")
  self.logo_sprite = sol.sprite.create("menus/title_zelda1/zelda_logo")
  self.logo_x, self.logo_y = 55,50
  
  self.black_surface = sol.surface.create(self.surface_w, self.surface_h)
  self.black_surface:fill_color({0,0,0,255})
  self.black_surface:set_opacity(100)
  
  self.intro_text = sol.surface.create("menus/title_zelda1/intro_text.png")
  self.all_of_treasures = sol.surface.create("menus/title_zelda1/all_of_treasures.png")
  self.read_the_manual = sol.surface.create("menus/title_zelda1/read_the_manual.png")
  self:start_phase_main()
end



function title:start_phase_main()  
  if not sol.menu.is_started(self) or self.finished then
    return
  end  
  
  self.phase = "main" --Other possible is "intro", which is the scrolling text
  self.black_surface:fade_out(20, function()
    sol.timer.start(10000, function()
      self.black_surface:fade_in(20, function()
        title:start_phase_intro()
      end)
    end)
  end)
end

function title:start_phase_intro()
  if not sol.menu.is_started(self) or self.finished then
    return
  end   
  
  self.phase = "intro"
  self.black_surface:set_opacity(0)
  self.intro_text:set_xy(0, self.surface_h)
  sol.timer.start(self, 2000, function()
    
    text_movement = sol.movement.create("straight")
    text_movement:set_angle(math.pi / 2)    
    text_movement:set_speed(30)
    text_movement:set_max_distance(self.surface_h)
    text_movement:start(self.intro_text, function()
      --At this point the intro text has moved onto the screen.
      sol.timer.start(2000, function()
        text_movement = sol.movement.create("straight")
        text_movement:set_angle(math.pi / 2)    
        text_movement:set_speed(30)
        text_movement:set_max_distance(self.surface_h)        
        text_movement:start(self.intro_text, function()
          self:start_phase_main()
        end)
      end)
    end)
  end)
end




function title:on_draw(dst_surface)
  if (self.phase == "main") then
    self:draw_main()
  end
  if (self.phase == "intro") then
    self:draw_phase()
  end
  self.surface:draw(dst_surface)
end

function title:draw_main()
  self.surface:clear()
  self.title_bg:draw(self.surface)
  self.logo_sprite:draw(self.surface, self.logo_x, self.logo_y)
  self.black_surface:draw(self.surface)
end

function title:draw_phase()
  self.surface:clear()
  self.intro_text:draw(self.surface)
  self.black_surface:draw(self.surface)
end

function title:on_key_pressed(key)

  if key == "escape" then
    -- Escape: quit Solarus.
    sol.main.exit()
    return true
  elseif not self.finished then
    self:skip_menu() 
    return true
  end
  
  return false
end

-- Skips the menu.
function title:skip_menu()
  if not sol.menu.is_started(self) or self.finished then
    return
  end

  -- Store the state.
  self.finished = true

  -- Quits after a fade to black.
  self.black_surface:fade_in(20, function()
    -- Quit the menu
    sol.menu.stop(self)
  end)
end


return title