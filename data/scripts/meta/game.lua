require("scripts/multi_events")

local game_meta = sol.main.get_metatable("game")

function game_meta:on_command_pressed(command)
  local hero = self:get_hero()
  local life = self:get_life()
  local max_life = self:get_max_life()

  if command == "attack" then
    if life == max_life and hero:get_sprite("sword") ~= nil then
      hero:create_sword_beam()
    end
--Stop the sword from loading.
    self:simulate_command_released("attack")
  end

  if command == "right" then
    if self:is_command_pressed("up") then
      self:simulate_command_released("up") 
    end    
    if self:is_command_pressed("down") then
      self:simulate_command_released("down")    
    end   
  end
  
  if command == "left" then
    if self:is_command_pressed("up") then
      self:simulate_command_released("up")    
    end    
    if self:is_command_pressed("down") then
      self:simulate_command_released("down")    
    end   
  end

  if command == "up" then
    if self:is_command_pressed("left") then
      self:simulate_command_released("left")    
    end    
    if self:is_command_pressed("right") then
      self:simulate_command_released("right")    
    end   
  end
  if command == "down" then
      if self:is_command_pressed("left") then
      self:simulate_command_released("left")    
    end    
    if self:is_command_pressed("right") then
      self:simulate_command_released("right")    
    end   
  end
end

--Here we add an on_command_released() function that will restore the original movement of the player.
--Example: If you were moving right and pressed up, you will move up. Upon releasing up, you will return
--to your original movment direction, which is right.
function game_meta:on_command_released(command)
  if command == "right" then
    if sol.input.is_key_pressed("up") then
      self:simulate_command_pressed("up") 

    end    
    if sol.input.is_key_pressed("down") then
      self:simulate_command_pressed("down")    
    end   
  end
  
  if command == "left" then
    if sol.input.is_key_pressed("up") then
      self:simulate_command_pressed("up")    
    end    
    if sol.input.is_key_pressed("down") then
      self:simulate_command_pressed("down")    
    end   
  end

  if command == "up" then
    if sol.input.is_key_pressed("left") then
      self:simulate_command_pressed("left")    
    end    
    if sol.input.is_key_pressed("right") then
      self:simulate_command_pressed("right")    
    end   
  end
  if command == "down" then
      if sol.input.is_key_pressed("left") then
      self:simulate_command_pressed("left")    
    end    
    if sol.input.is_key_pressed("right") then
      self:simulate_command_pressed("right")    
    end   
  end
end


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------Update HUD elements such as the heart containers.-------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------
local function update_entities(game)
  if game ~= nil then
    if game:get_map() ~= nil then
      local map = game:get_map()  
      for entity in map:get_entities_by_type("pickable") do
        local sprite = entity:get_sprite()        
        if sprite:get_animation() == "consumables/piece_of_heart" then        
          local nb_pieces_of_heart = game:get_value("i1030") or 0
          sprite:set_direction(nb_pieces_of_heart)
        end
      end
    end
  end
end


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------Setting for the camera. Called in game:on_started().----------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------
local function set_camera(game)
  if game ~= nil then
    if game:get_map() ~= nil then
      local map = game:get_map()  
      local camera = map:get_camera()
      camera:set_size(256, 176)
      --camera:set_position_on_screen(0, 0)
    end
  end
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------Register 'game' events here.----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------
game_meta:register_event("on_map_changed", set_camera)
game_meta:register_event("on_map_changed", update_entities)