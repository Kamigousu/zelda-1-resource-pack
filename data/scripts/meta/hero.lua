local hero_meta = sol.main.get_metatable("hero")
local m = sol.movement.create("straight")
local sword_beam


function hero_meta:create_sword_beam()
  local hero = self
  local map = hero:get_map()
  local dir = hero:get_direction()
  local h_x, h_y, l = hero:get_position()
  local x, y = h_x, h_y
  local angle
  local speed

  if map:has_entity("sword_beam") then return end

  if dir == 0 or dir == 2 then  --Hero is facing East/Right or West/Left.
    if dir == 0 then
      x = h_x + 4
      angle = dir
    else
      x = h_x - 4
      angle = math.pi
    end
  elseif dir == 1 or dir == 3 then --Hero is facing North/Up or South/Down.
    if dir == 1 then
      y = h_y - 4
      angle = math.pi / 2
    else
      y = h_y + 4
      angle = 3 * math.pi / 2
    end
  end

  local sword_beam_props = 
  {
    ["name"] = "sword_beam",
    ["direction"] = dir,
    ["layer"] = l,
    ["x"] = x,
    ["y"] = y,
    ["width"] = 16,
    ["height"] = 16,
    ["sprite"] = "hero/sword_beam",
    ["model"] = nil,
    ["enabled_at_start"] = false,
    ["properties"] = nil,
  }
  sword_beam = map:create_custom_entity(sword_beam_props)
  speed = 196
  self:fire_sword_beam(angle, speed)
end


function hero_meta:fire_sword_beam(angle, speed)
  local hero = self
  local sword = hero:get_sprite("sword")
  local sword_id = hero:get_sword_sprite_id()
  local damage
  --Define the custom properties of the sword beam.
  sword_beam:set_can_traverse_ground("wall", true)
  sword_beam:set_can_traverse_ground("deep_water", true)
  sword_beam:set_can_traverse_ground("shallow_water", true)
  sword_beam:set_can_traverse(true)

  --sword_beam:set_can_traverse("enemy", false)

  sword_beam:set_layer_independent_collisions()

  if sword_id == " hero/sword1" then
    damage = 1
  elseif sword_id == "hero/sword2" then
    damage = 2
  else
    damage = 3
  end


  sword_beam:add_collision_test("sprite", function(beam, entity)
    if entity:get_type() == "enemy" then
      --Ignore projectiles created by enemies.
      if entity:get_breed() == "others/octorok_stone" then return end
      entity:hurt(damage)
      hero_meta:explode_sword_beam()
    end
  end)
  sword_beam:add_collision_test("overlapping", function(beam, entity)
    if entity:get_type() == "separator" then
      hero_meta:explode_sword_beam()
    end
  end)

  function sword:on_animation_finished(animation)
    if animation == "sword" then
      sword_beam:set_enabled()
      m:set_speed(speed)
      m:set_angle(angle)
      m:start(sword_beam)
    end
  end
end


function hero_meta:explode_sword_beam()
  local sprite = sword_beam:get_sprite()
  --Start the explosion animation.
print("once")
  m:stop()
  sprite:set_animation("explosion")
  function sprite:on_animation_finished(animation)
    sword_beam:remove()
  end

end

function m:on_obstacle_reached()
  --sword_beam:remove()
  --hero_meta:explode_beam()
end